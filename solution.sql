CREATE DATABASE blog_db;

CREATE TABLE users (
    id INT NOT NULL AUTO_INCREMENT,
    email VARCHAR (100) NOT NULL,
    password VARCHAR (300) NOT NULL,
    date_time_created DATETIME,
    PRIMARY KEY (id)
)

CREATE TABLE posts (
    id INT NOT NULL AUTO_INCREMENT,
    author_id INT NOT NULL,
    title VARCHAR (500) NOT NULL,
    content VARCHAR (5000) NOT NULL,
    date_time_posted DATETIME,
    PRIMARY KEY (id)
)

CREATE TABLE post_likes (
    id INT NOT NULL AUTO_INCREMENT,
    post_id INT NOT NULL,
    user_id INT NOT NULL,
    date_time_created DATETIME,
    PRIMARY KEY (id),
    CONSTRAINT fk_likes_post_id
        FOREIGN KEY (post_id) REFERENCES posts(id)
        ON UPDATE CASCADE
        ON DELETE RESTRICT,
    CONSTRAINT fk_likes_user_id
        FOREIGN KEY (user_id) REFERENCES users(id)
        ON UPDATE CASCADE
        ON DELETE RESTRICT
)

CREATE TABLE post_comments (
    id INT NOT NULL AUTO_INCREMENT,
    post_id INT NOT NULL,
    user_id INT NOT NULL,
    content VARCHAR(5000) NOT NULL,
    PRIMARY KEY (id),
    CONSTRAINT fk_comments_post_id
        FOREIGN KEY (post_id) REFERENCES posts(id)
        ON UPDATE CASCADE
        ON DELETE RESTRICT,
    CONSTRAINT fk_comments_user_id
        FOREIGN KEY (user_id) REFERENCES users(id)
        ON UPDATE CASCADE
        ON DELETE RESTRICT
)









-- [SECTION] Inserting/Creating Records

INSERT INTO users (email, password, date_time_created) VALUES (
	"johnsmith@gmail.com",
	"passwordA",
	"2021-01-01 01:00:00"
);
INSERT INTO users (email, password, date_time_created) VALUES (
	"juandelacruz@gmail.com",
	"passwordB",
	"2021-01-01 02:00:00"
);
INSERT INTO users (email, password, date_time_created) VALUES (
	"janesmith@gmail.com",
	"passwordC",
	"2021-01-01 03:00:00"
);
INSERT INTO users (email, password, date_time_created) VALUES (
	"mariadelacruz@gmail.com",
	"passwordD",
	"2021-01-01 04:00:00"
);
INSERT INTO users (email, password, date_time_created) VALUES (
	"johndoe@gmail.com",
	"passwordE",
	"2021-01-01 05:00:00"
);


INSERT INTO posts (user_id, title, content, date_time_posted) VALUES (
	1,
    "First Code",
    "Hello World",
    "2021-01-01 01:00:00"
);
INSERT INTO posts (user_id, title, content, date_time_posted) VALUES (
	2,
    "Second Code",
    "Hello Earth",
    "2021-01-01 02:00:00"
);
INSERT INTO posts (user_id, title, content, date_time_posted) VALUES (
	3,
    "Third Code",
    "Welcome to Mars",
    "2021-01-01 03:00:00"
);
INSERT INTO posts (user_id, title, content, date_time_posted) VALUES (
	4,
    "Fourth Code",
    "Bye Solar System",
    "2021-01-01 04:00:00"
);

SELECT * FROM posts WHERE author_id = 1;

SELECT email, datetime_created FROM users;

UPDATE posts SET content = "Hello to the people of Earth" WHERE id = 2;

DELETE users WHERE id = 4;

DELETE users WHERE email = "johndoe@gmail.com";